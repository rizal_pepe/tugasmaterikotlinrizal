package com.example.tugasmaterikotlin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn_go.setOnClickListener {
            val usernames = username.text.toString()
            if (usernames.isNotEmpty()) {
                Konstanta.setusername(username.text.toString())
                Konstanta.setgenre(genrespin.selectedItem.toString())
                Toast.makeText(this, "Telah Login", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Username Kosong", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
