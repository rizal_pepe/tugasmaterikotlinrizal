package com.example.tugasmaterikotlin

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Konstanta.getgenre().equals("LAKI-LAKI", true)) {
            nama.text = Konstanta.getusername()
            gen.text = Konstanta.getgenre()
            nama.setTextColor(Color.GREEN)
            gen.setTextColor(Color.GREEN)
        } else {
            nama.text = Konstanta.getusername()
            gen.text = Konstanta.getgenre()
            nama.setTextColor(Color.rgb(255, 0, 102))
            gen.setTextColor(Color.rgb(255, 0, 102))
        }
        btnLogout.setOnClickListener {
            Konstanta.setusername("")
            Konstanta.setgenre("")
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, Login::class.java)
            startActivity(intent)
            finish()
        }
    }
}
