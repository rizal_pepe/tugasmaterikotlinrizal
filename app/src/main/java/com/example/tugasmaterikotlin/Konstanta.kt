package com.example.tugasmaterikotlin

class Konstanta {
    companion object {
        var username = "Siapa"
        fun setusername(username: String) {
            this.username = username
        }

        fun getusername(): String {
            return username
        }

        var genre = "Laki"
        fun setgenre(genre: String) {
            this.genre = genre
        }

        fun getgenre(): String {
            return genre
        }
    }
}